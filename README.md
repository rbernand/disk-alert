# Disk Alert

Disk alert is a cli/daemon to send notification when disk space reaches a threshold.
Avaialble services ti be notified:
- Discord Webhook
- Gotify

## Usage
### One shot CLI
```
disk-alert / 90
Disk usage: 98.83%
```
The threshold is reached, but disk-will send no notifications beaucase no reporters is provided. Add one of `--report gotify` or `--report discord`. Environement variables are required.
