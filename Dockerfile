FROM python:3.12 as builder

RUN python -m venv /opt/disk-alerting
ENV PATH="/opt/disk-alerting/bin:$PATH"

COPY . .

RUN pip install .


FROM python:3.12-alpine

COPY --from=builder /opt/disk-alerting /opt/disk-alerting
ENTRYPOINT ["/opt/disk-alerting/bin/disk-alerting"]
