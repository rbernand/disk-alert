from dataclasses import dataclass


@dataclass
class DiskUsage:
    total: int
    used: int
    free: int

    @property
    def percentage_use(self):
        return (self.used / self.total) * 100

    def __str__(self):
        return f"Disk usage: {self.percentage_use:.2f}%"
