import os
from urllib.parse import urljoin

import requests

from disk_alerting.disk_usage import DiskUsage
from disk_alerting.reporters.base import Reporter


class GotifyReporter(Reporter):
    name = "gotify"

    def __init__(self):
        try:
            self.url = os.environ["GOTIFY_HOST_URL"]
            self.api_key = os.environ["GOTIFY_API_KEY"]
            self.hostname = os.uname()[1]
        except KeyError as err:
            print(err, "missing")
            raise

    def send_disk_alert(self, partition, disk_usage: DiskUsage):
        self._send_message(f"Disk full on {partition}", 1, str(disk_usage))

    def send_end_alert(self, partition, disk_usage: DiskUsage):
        self._send_message(f"End Alert on {partition}", 5, str(disk_usage))

    # TODO: change priority
    def _send_message(self, title: str, priority: int, message: str):
        ret = requests.post(
            urljoin(self.url, "message"),
            headers={"Authorization": f"Bearer {self.api_key}"},
            json={"title": title, "priority": priority, "message": message},
        )
        ret.raise_for_status()
