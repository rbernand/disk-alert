import enum
import os

import requests

from disk_alerting.disk_usage import DiskUsage
from disk_alerting.reporters.base import Reporter


class DiscordReporter(Reporter):
    name = "discord"

    class Priority(enum.Enum):
        DANGER = 16056320
        WARNING = 16754176
        OK = 54314

    def __init__(self):
        try:
            self.webhook = os.environ["DISCORD_WEBHOOK"]
            self.hostname = os.uname()[1]
        except KeyError as err:
            print(err, "missing")
            raise

    def send_disk_alert(self, partition: str, disk_usage: DiskUsage):
        self._send_message(
            f"Disk full on {partition}", self.Priority.DANGER, str(disk_usage)
        )

    def send_end_alert(self, partition: str, disk_usage: DiskUsage):
        self._send_message(
            f"End Alert on {partition}", self.Priority.OK, str(disk_usage)
        )

    def _send_message(
        self, title: str, priority: "DiscordReporter.Priority", message: str
    ):
        ret = requests.post(
            self.webhook,
            params={"wait": True},
            json={
                "attachement": [],
                "avatar_url": None,
                "content": "Disk full",
                "embeds": [
                    {
                        "color": priority.value,
                        "description": message,
                        "title": title,
                    }
                ],
                "username": self.hostname,
            },
        )
        ret.raise_for_status()
