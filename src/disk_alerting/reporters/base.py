import abc
import enum

from disk_alerting.disk_usage import DiskUsage


class Reporter(abc.ABC):
    class Priority(enum.Enum):
        DANGER = 1
        WARNING = 2
        OK = 3

    @abc.abstractmethod
    def send_disk_alert(self, partition: str, disk_usage: DiskUsage): ...
    @abc.abstractmethod
    def send_end_alert(self, partition: str, disk_usage: DiskUsage): ...

    def _convert_bytes(self, size: float):
        """Converts the input size (in KB) into a more readable format"""
        for x in ["KB", "MB", "GB", "TB"]:
            if size < 1024.0:
                return f"{size:3.1f}{x}"
            size /= 1024.0
        return f"{size:,}"
