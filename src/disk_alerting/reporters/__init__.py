from .gotify import GotifyReporter
from .discord import DiscordReporter


Reporters = {
    GotifyReporter.name: GotifyReporter,
    DiscordReporter.name: DiscordReporter,
}


class ReportersGroup:
    def __init__(self, *reporter_names):
        self.reporters = []
        for reporter_name in reporter_names:
            self.reporters.append(Reporters[reporter_name]())

    def send_all_disk_alert(self, partition, disk_usage):
        for reporter in self.reporters:
            reporter.send_disk_alert(partition, disk_usage)

    def send_all_end_alert(self, partition, disk_usage):
        for reporter in self.reporters:
            reporter.send_end_alert(partition, disk_usage)
