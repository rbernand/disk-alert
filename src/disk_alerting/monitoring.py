import shutil

from disk_alerting.disk_usage import DiskUsage
from disk_alerting.reporters import ReportersGroup


class DiskMonitor:
    def __init__(self, partition: str, threshold: int):
        self.partition: str = partition
        self.threshold: int = threshold
        self.threshold_reached: bool = False

    def _get_disk_space(self):
        total, used, free = shutil.disk_usage(self.partition)
        return DiskUsage(total, used, free)

    def monitor(self, reporters: ReportersGroup):
        disk_usage = self._get_disk_space()
        message = None
        if disk_usage.percentage_use >= self.threshold:
            if not self.threshold_reached:
                message = "Alert disk full"
                reporters.send_all_disk_alert(self.partition, disk_usage)
            self.threshold_reached = True
        else:
            if self.threshold_reached:
                message = "End Alert"
                reporters.send_all_end_alert(self.partition, disk_usage)
            self.threshold_reached = False
        if message:
            print(str(disk_usage), f"- {message}")
        else:
            print(str(disk_usage))
