import click
import daemon as daemon_lib  # type: ignore
import sys
import time

from disk_alerting.reporters import Reporters, ReportersGroup
from disk_alerting.monitoring import DiskMonitor


@click.command()
@click.option("--daemon/--no-daemon", default=False, help="Start a daemon process.")
@click.option(
    "-t",
    "--interval",
    type=int,
    default=60,
    help="Interval in seconds for the daemon process.",
)
@click.option(
    "--attach/--detach",
    default=False,
    help="Attach or detach the daemon process child.",
)
@click.option(
    "--report",
    multiple=True,
    type=click.Choice(list(Reporters.keys())),
    help="define a service to notify. One of",
)
@click.argument(
    "partition",
    # help="",
)
@click.argument(
    "threshold",
    type=int,
    # help="",
)
def main(
    partition,
    threshold: int,
    daemon: bool,
    interval: int,
    attach: bool,
    report: list[str],
):
    reporters = ReportersGroup(*report)
    if daemon:
        disk_monitor = DiskMonitor(partition, threshold)
        with daemon_lib.DaemonContext(
            detach_process=not attach, stdout=sys.stdout if attach else None
        ):
            while True:
                disk_monitor.monitor(reporters)
                time.sleep(interval)
    else:
        DiskMonitor(partition, threshold).monitor(reporters)


if __name__ == "__main__":
    main()
